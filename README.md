**Biological Networks Project in Python**

This project is a python lesson in Master 2 bioinformatics. It consists of studying graphs of interactions between proteins and it's organized in 6 parts.

1. Reading a graph of interactions between proteins

2. Exploration of the protein-protein interactions graph

3. Modification of specifications, object oriented python

4. Calculate the components of a graph of interactions between proteins

5. Search domains which compose proteins

6. Composition analysis in interactome domains

#!/usr/bin/python3
# encoding: utf-8

import pickle
import matplotlib.pyplot as plt

complete_file = "C:\\Users\\adama\\OneDrive\\bs2_projet_2020\\Files\\Human_HighQuality.txt"
test_file = "C:\\Users\\adama\\OneDrive\\bs2_projet_2020\\Files\\toy_example.txt"

class DomainGraph():
    """
    Attributs
    """
    int_dict = {}
    proteins_list = []
    domains_list = []
    dict_graphe_final = {} 

    """
    constructor
    """
    def __init__(self, graph_file):
        self.int_dict = self.read_interaction_file_dict(graph_file)
        self.proteins_list = self.ls_proteins()
        self.domains_list = self.ls_domains()
        self.dict_graphe_final = self.load_dict_graphe_final()


    def read_interaction_file_dict(self, graph_file):
        """
        Reads a graph of interactions between proteins and stores it in a dictionary
        :param: File containing interaction graphs
        :return: dict
        """
        dict_interaction = {}
        file = open(graph_file, "r")
        data = file.readlines()[1:]

        for line in data:
            new_data = line.split()

            summit = new_data[0]
            summit_neighboor = new_data[1]

            if not (summit in dict_interaction):
                dict_interaction[summit] = set()
                dict_interaction[summit].add(summit_neighboor)

            if (summit in dict_interaction):
                dict_interaction[summit].add(summit_neighboor)

            if not (summit_neighboor in dict_interaction):
                dict_interaction[summit_neighboor] = set()
                dict_interaction[summit_neighboor].add(summit)

            if (summit_neighboor in dict_interaction):
                dict_interaction[summit_neighboor].add(summit)

        file.close()
        return(dict_interaction)

    def ls_proteins(self):
        """
        Returns a list of all the proteins present in the interaction graph protein
        :rtype: list
        """    
        protein_list = []
        
        #get proteins in int_dict
        for key in self.int_dict.keys():
            if not key in protein_list:
                protein_list.append(key)
            
        return protein_list
    
    def ls_domains(self):
        """
        Returns a list of all the domains present in the interaction graph protein
        :rtype: list
        """
        domains_list = []
        dict_graphe = pickle.load( open("dict_graphe_final.pkl","rb"))
        
        #get domains in dict_graph
        for key, val in dict_graphe.items():
                if val['domains']:
                    for domain in (val['domains']):
                        if not domain in domains_list:
                            domains_list.append(domain)
        return domains_list 
                    
                    
    def load_dict_graphe_final(self):
        """
        load the dictionary store in the pickle file to avoid calling the xlinkdomains function each time
        """
        
        with open("Results_files/dict_graphe_final.pkl", "rb") as pklFile:
            return(pickle.load(pklFile))
        
        
    def  co_occurrence(self, dom_x, dom_y):
        """
        calculates and returns the number of co-occurrences of the x and y domains in the proteins of the interactome
        :param a: dom_x
        :param b: dom_y
        :rtype: int
        """
        nb_cooccurrence = 0        
        dict_graphe = pickle.load( open("dict_graphe_final","rb")) # prot, neighboors, uniprotID and domains
        for protein in dict_graphe.keys():
            if dom_x in dict_graphe[protein]["domains"] and dom_y in dict_graphe[protein]["domains"]: # verification if the 2 domains are presents in the prot
                nb_cooccurrence += 1
        return(nb_cooccurrence)         
        
    def generate_cooccurrence_graph(self):
        """
        returns a new graph, whose vertices are the domains. Knowing that we have an interaction between the vertices x and y
        only when these two domains are co-occurring in at least one protein of the graph.
        """
        dict_graphe = self.load_dict_graphe_final()
        dict_cooccurence = {}
        for protein in dict_graphe.keys():
            domains_list = (dict_graphe[protein]['domains'])
            if domains_list:
                if len(domains_list) > 1 : #verify protein with more than one domain
                    for dom_x in domains_list :
                        #print(dom_x)
                        for dom_y in domains_list:
                            #print(dom_y)
                             if dom_y != dom_x:
                                if dom_x not in dict_cooccurence:
                                    dict_cooccurence[dom_x] = {dom_y:0}
                                    #print(dict_cooccurence[dom_x])
                                if dom_y not in dict_cooccurence:
                                    dict_cooccurence[dom_y] = {dom_x:0}
                                    #print(dict_cooccurence[dom_y])
                                if dom_x not in dict_cooccurence[dom_y]:
                                    dict_cooccurence[dom_y] = {dom_x:0}
                                    #print(dict_cooccurence[dom_y])
                                if dom_y not in dict_cooccurence[dom_x]:
                                    dict_cooccurence[dom_x] = {dom_y:0}
                              
                                dict_cooccurence[dom_x][dom_y] += 1
                                dict_cooccurence[dom_y][dom_x] += 1

        return(dict_cooccurence)
    
    def clean_cooccurrence_graph(self):
        """
        create a couple list without redundancy of domains interactions
        :rtype: tuple
        """
        list_couple_domain = []      
        dict_cooccurence = self.generate_cooccurrence_graph()
        
        for dom_x, val in dict_cooccurence.items():
            #print(dom_x)
            for dom_y in val:
                if dom_x != dom_y:
                    list_couple_domain.append((dom_x, dom_y))
        #print(list_couple_domain) # ('PF12026', 'PF00018'), ('PF08824', 'PF00018')
        # remove redundant interactions
        for couple1 in list_couple_domain:
            for couple2 in list_couple_domain:
                if couple1[0] == couple2[1] and couple1[1] == couple2[0]:
                    list_couple_domain.remove(couple2)        
        
        return(list_couple_domain)
    
    
    def get_dict_cooccurrence_clean(self):
        """
        transform the previous tuple into a dictionary
        :rtype: dictionnary
        """
        list_couple_domain = self.clean_cooccurrence_graph()
        #print(list_couple_domain)
        DomainGraph = {}
        with open("DomainGraph.pkl", "wb") as pklFile:
            for couple in list_couple_domain:
                if not couple[0] in DomainGraph:
                    DomainGraph[couple[0]] = []
                    DomainGraph[couple[0]].append(couple[1])
                    
                if couple[0] in DomainGraph and couple[1] not in DomainGraph[couple[0]]:
                    DomainGraph[couple[0]].append(couple[1])
                    
                if not couple[1] in DomainGraph:
                    DomainGraph[couple[1]] = []
                    DomainGraph[couple[1]].append(couple[0])
                    
                if couple[1] in DomainGraph and couple[0] not in DomainGraph[couple[1]] :
                    DomainGraph[couple[1]].append(couple[0])
            pickle.dump(DomainGraph, pklFile)
            
        return DomainGraph


    def density(self):
        """
        Calculates the density of the Domaingraph
        :return: The density
        :rtype: float
        """
        edges_presents = 2 * len(self.clean_cooccurrence_graph())
        theoreticale_edges = (len(self.get_dict_cooccurrence_clean()) * (len(self.get_dict_cooccurrence_clean()) - 1))
        density_d = edges_presents / theoreticale_edges
        return density_d


    def get_dom_highest_neighbors(self):
        """
        Returns the 10 domains with the largest number of neighbors
        :rtype: list
        """
        dict_dom_nb_neighbors = {}
        list_domain = []
        DomainGraph = self.get_dict_cooccurrence_clean()
        
        with open("dom_highest_neighbors.pkl", "wb") as pklFile:
            for dom, neigh_dom in DomainGraph.items():
                #get the number of neighbors of each domain
                dict_dom_nb_neighbors[dom] = len(neigh_dom)
                # sorts dictionary values ​​from smallest to largest and returns the 10 last
                dict_dom_nb_neighborsBis = sorted(dict_dom_nb_neighbors.items(), key=lambda x: x[1], reverse=True)[:10]
            for domain in dict_dom_nb_neighborsBis:
                print(domain)
                list_domain.append(domain[0])                
            pickle.dump(list_domain, pklFile)
                       
        return list_domain
        
          
    def get_dom_lowest_neighbors(self):
        """
        Returns the 10 domains with the lowest number of neighbors
        :rtype: list
        """
        dict_dom_nb_neighbors = {}
        list_domain = []
        DomainGraph = self.get_dict_cooccurrence_clean()
        with open("dom_lowest_neighbors.pkl", "wb") as pklFile:
            for dom, neigh_dom in DomainGraph.items():
                #get the number of neighbors of each domain
                dict_dom_nb_neighbors[dom] = len(neigh_dom)
                # sorts dictionary values ​​from smallest to largest and returns the first 10
                dict_dom_nb_neighborsBis = sorted(dict_dom_nb_neighbors.items(), key=lambda x: x[1])[:10]
            for domain in dict_dom_nb_neighborsBis:
                print(domain)
                list_domain.append(domain[0])
            pickle.dump(list_domain, pklFile)
        return list_domain

    
    
interactome_file = DomainGraph(complete_file)
test_interactome = DomainGraph(test_file)

#print(interactome_file.int_dict)
#print(interactome_file.proteins)
#print(interactome_file.load_dict_graphe_final())
#print(interactome_file.ls_domains())
#print(interactome_file.co_occurrence('PF06741', 'PF07145'))
#print(interactome_file.generate_cooccurrence_graph())
#print(interactome_file.clean_cooccurrence_graph())
#print(interactome_file.get_dict_cooccurrence_clean())
#print(interactome_file.count_edges())
#print(interactome_file.count_vertices())
#print("Density", interactome_file.density())
#print(interactome_file.get_dom_highest_neighbors())
print(interactome_file.get_dom_lowest_neighbors())
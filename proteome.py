#!/usr/bin/python3
# encoding: utf-8


uniprot_file = "C:\\Users\\adama\\OneDrive\\bs2_projet_2020\\Files\\uniprot_test.tab"

class Proteome():
    """ Attributs """
    
    prot_names_list = []
    protID_prot_dict = {}
    prot_protID_dict = {}
   
    
    """ constructor """
    def __init__(self, proteom_file):
        self.prot_names_list = self.prot_names_list(proteom_file)
        self.protID_prot_dict = self.protID_prot_dict(proteom_file)
        self.prot_protID_dict = self.prot_protID_dict(proteom_file)
        
        

    def prot_names_list(self, proteom_file):
        list_prot_proteom = []
        file = open(proteom_file, "r")
        data = file.readlines()[1:]

        for line in data:
            prot_names = line.split()[1]
            list_prot_proteom.append(prot_names)
        return list_prot_proteom
    
    
    
    def protID_prot_dict(self, proteom_file):
        dict_prot_proteom = {}
        file = open(proteom_file, "r")
        data = file.readlines()[1:]
        for line in data:
            protID = line.split()[0]
            prot_names = line.split()[1]
        
            if not protID in dict_prot_proteom:
                dict_prot_proteom[protID] = prot_names      
        return dict_prot_proteom
    
    
    def prot_protID_dict(self, proteom_file):
        dict_prot_proteom = {}
        file = open(proteom_file, "r")
        data = file.readlines()[1:]
        for line in data:
            protID = line.split()[0]
            prot_names = line.split()[1]
            
            if not prot_names in dict_prot_proteom:
                dict_prot_proteom[prot_names] = protID      
        return dict_prot_proteom


file_proteom = Proteome(uniprot_file)
#print(file_proteom.prot_names_list)
#print(file_proteom.protID_prot_dict)
#print(file_proteom.prot_protID_dict)


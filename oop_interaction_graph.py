#!/usr/bin/python3
# encoding: utf-8

########################################
##### Proteins' interactions graph #####
########################################

import tempfile
import urllib.request
import re
import ssl
import pickle
import sys
import os
import matplotlib.pyplot as plt
import proteome



#graph_file = 'toy_example.txt'
uniprot_file = "C:\\Users\\adama\\OneDrive\\bs2_projet_2020\\Files\\uniprot_test.tab"
test_proteom = proteome.Proteome(uniprot_file)

class Interactome():
    """
    Attributs
    """
    int_dict = {}
    int_list = []
    proteins = []

    """
    constructor
    """
    def __init__(self, graph_file):
        self.int_dict = self.read_interaction_file_dict(graph_file)
        self.int_list = self.read_interaction_file_list(graph_file)
        self.proteins = self.proteins_list(graph_file)

    ### week1: Reading a graph interactions between proteins ###

    def proteins_list(self, file):
        """
        :param : file containing interaction graphs
        :return: list of proteins contained in the graph
        :rtype: list
        """
        summit_list = []
        for protein in self.int_dict.keys():
            summit_list.append(protein)
        return(summit_list)

    def read_interaction_file_dict(self, graph_file):
        """
        Reads a graph of interactions between proteins and stores it in a dictionary
        :param: File containing interaction graphs
        :return: dict
        """
        dict_interaction = {}
        file = open(graph_file, "r")
        data = file.readlines()[1:]

        for line in data:
            new_data = line.split()

            summit = new_data[0]
            summit_neighboor = new_data[1]

            if not (summit in dict_interaction):
                dict_interaction[summit] = set()
                dict_interaction[summit].add(summit_neighboor)

            if (summit in dict_interaction):
                dict_interaction[summit].add(summit_neighboor)

            if not (summit_neighboor in dict_interaction):
                dict_interaction[summit_neighboor] = set()
                dict_interaction[summit_neighboor].add(summit)

            if (summit_neighboor in dict_interaction):
                dict_interaction[summit_neighboor].add(summit)

        file.close()
        return(dict_interaction)

    def read_interaction_file_list(self, graph_file):
        """
        Reads a graph of interactions between proteins and stores it in a couple list
        :param: File containing interaction graphs
        :rtype: list
        """
        
        list_interaction = []
        file = open(graph_file, "r")
        data = file.readlines()[1:]

        for line in data:
            new_data = line.split()

            summit = new_data[0]
            summit_neighboor = new_data[1]
            couples = (summit, summit_neighboor)
            list_interaction.append(couples)

        file.close()
        return(list_interaction)


    ### week2: Exploration of the graph protein interactions ###

    def count_vertices(self):
        """
        Counts the number of summits of a Graph
        :return: Number of summits
        :rtype: int
        """
        summit_number = len(self.int_dict)
        #print(self.int_dict)
        return summit_number


    def count_edges(self):
        """
        counts the number of edges of a graph
        :return: number of edges
        :rtype: int
        """
        summit_neighbour_number = len(self.int_list)
        #print(self.int_list)
        return summit_neighbour_number
    

    def clean_interactome(self):
        """
        reads a file containing a graph interactions and filters all redundant interactions
        :return: file containing the new filtered graph
        """
        interactions_list = []
        for interaction in self.int_list:
            prot_interaction = (interaction[1], interaction[0])
            
            # verify if interaction not in interaction list and Adding it 
            if interaction not in interactions_list:
                if prot_interaction not in interactions_list:
                    if interaction[1] != interaction[0]:
                        interactions_list.append(interaction)
        interactions_number = str(len(interactions_list)) # for the first line of the output file

        outfile = open('file_interaction_filter.txt', 'w') # create the output file
        outfile.write(str(interactions_number + '\n'))  # write the first line

        for interaction in interactions_list:
            interaction_outfile = interaction[0], interaction[1]
            new_interaction_outfile = '\t'.join(interaction_outfile) # return the interaction_outfile variable by separating it with a tabulation
            outfile.write(str(new_interaction_outfile + '\n')) 
        outfile.close()


    def get_degree(self, prot):
        """
        :param: Protein
        :return: The degree of this protein in the graph
        :rtype: int
        """
        degree_int = 0
        if prot in self.int_dict.keys():
            degree_int = len(self.int_dict[prot]) # its number of interaction
        return degree_int
    

    def get_max_degree(self):
        """
        calculated the degree of each protein and returns the max degree
        :return : Degree max
        :rtype: int
        """
        max_degree = ("", 0)  #the variable max degree stores the protein and its score. 
        for summit in self.int_dict:
            degree = len(self.int_dict[summit]) # calculate degre of each protein

            if degree > max_degree[1]:
                max_degree = (summit, degree) #It replaces the degree with the largest value on each loop revolution
            if (degree == max_degree[1]) & (summit != max_degree[0]): # check if we have several max_degree
                max_degree = max_degree + (summit, degree)
        return max_degree
    

    def get_ave_degree(self):
        """
        Calculates the average protein degree graph
        :return: Average protein degree
        :rtype: int
        """
        all_degrees = 0
        number_of_degree = 0
        
        for summit in self.int_dict:
            all_degrees += len(self.int_dict[summit])
            number_of_degree = len(self.int_dict)
        ave_degree = (all_degrees/number_of_degree)
        
        return ave_degree
    

    def histogram_degree(self, dmin, dmax):
        """
        calculates for all the degrees d between dmin and dmax, the number of proteins having a degree d
        :param a: degree min
        :param b: degree max
        """
        prot_number = 0
        for summit in self.int_dict:
            if dmin <= len(self.int_dict[summit]) <= dmax: 
                prot_number += 1
        degree = dmin
        while degree <= dmax:
            degree += 1
            hist = str(degree) + " "
            for summit in self.int_dict:
                if len(self.int_dict[summit]) == degree:
                    hist += "*"
            print(hist)
        return prot_number


    ### week4: Component calculation of a graph of interactions between proteins ###

    def extractCC(self, prot):
        """
        Returns all the vertices of the related component of prot
        :param: protein
        :rtype: list
        """       
        # here I used the BFS algorithm which allows me to iterate through all
        # the vertices of a connected component
        
        visited_dict = {}
        queue_list = []
        queue_list.append(prot)
        summits_str = prot  # allows to concatenate the traversed summits
        visited_dict[prot] = True
        while queue_list:
            prot = queue_list.pop(0) # Removes the first element from the queue_list and stores it in the prot variable
            for node in self.int_dict[prot]:
                if visited_dict.get(node, False) == False:   # check that the node key is not in the visited dictionary
                    queue_list.append(node)
                    visited_dict[node] = True
                    summits_str += '/' + node
        return summits_str.split("/")


    def get_ccs_dict(self):
        """
        Returns a list containing all the connected component
        """
        ccs_dict = []
        for prot in self.proteins:
            summits_list = self.extractCC(prot)  
            summits_list.sort()
            if summits_list not in ccs_dict:
                ccs_dict.append(summits_list)
            #print(len(ccs_dict))
        return ccs_dict
    
    def countCC(self):
        """
        Calculates the number of related components of a graph and gives for one of it its size
        """
        ccs_list = self.get_ccs_dict()
        number_ccs = 0
        
        for cc in ccs_list:
            number_ccs += 1
            print("the connected component number ", str(number_ccs), " has ", str(len(cc)), " proteins ")
            
        return(len(ccs_list)) # total number of cc in the graph


    def writeCC(self):
        """
        Writes the various related components of a graph to an output file
        :return: file containing the differents components of the graph
        """
        outfile = open('file_ccs.txt', 'w')
        for summits in self.get_ccs_dict():
            new_cc_outfile = str(len(summits)) + '\t' + ', '.join(summits)
            outfile.write(new_cc_outfile + '\n')
        outfile.close()


    def computeCC(self):
        """
        Corresponds to a list lcc where each lcc [i] element corresponds to the related
        component number of the protein at position i in the list of proteins in the graph
        :rtype: list
        """
        lcc_list = [-1] * self.count_vertices()
        print(lcc_list)
        for cc_index, summits in enumerate(self.get_ccs_dict()):
            #print(cc_index)
            for i in range(self.count_vertices()):
                if self.proteins[i] in summits:
                    lcc_list[i] = cc_index
        return lcc_list
    

    def density(self):
        """
        Calculates the density of the graph
        :return: The density
        :rtype: float
        """
        edges_presents = 2 * self.count_edges()
        theoreticale_edges = (self.count_vertices() * (self.count_vertices() - 1))
        density_d = edges_presents / theoreticale_edges
        return density_d

    ####### week5: Search domains which make up proteins ##########

# Questions préliminaires

#Q1: La base de donnée pfam contient une large collection de familles de protéines 

#Q2: Identifiant de INSR_HUMAN : P06213

#Q3 : Les domaines protéiques dans pfam : Recep L domain, Furin like, Recep L domain, Insulin TMD, PK TyrSER Thr
 #Les domaines protéiques dans pfam :Fibronectin type-III (1), Fibronectin type-III (2),Fibronectin type-III (3), Protein kinase

    def xlinkUniprot(self):
        """
        Extend the dictionary storing the interactome which for each protein
        It provides information for each interactome protein identifying it as well as its neighbors
        :rtype: return
        """
        dict_graphe = {}
        count_absents_prot = 0
        count_presents_prot = 0
        
        with open("dict_graphe_uniprot.pkl", "wb") as pklFile:
            for protein in interactome_file.int_dict:
                if not protein in test_proteom.prot_protID_dict:
                    dict_graphe[protein] = {'neighbours': self.int_dict[protein], 'uniprotID': 'Absent'}
                    
                    if dict_graphe[protein]['uniprotID'] == 'Absent':
                        count_absents_prot += 1
                              
                else:
                    dict_graphe[protein] = {'neighbours': self.int_dict[protein], 'uniprotID': test_proteom.prot_protID_dict[protein]}
                    if dict_graphe[protein]['uniprotID'] != 'Absent':
                        count_presents_prot += 1                    
            pickle.dump(dict_graphe, pklFile)
            # nom_variable = pickle.load(open('nom_fichier.pkl','rb'))
            
            print("Il y'a " + str(count_absents_prot) + " protéines absentes")
            print("Il y'a " + str(count_presents_prot) + " protéines présentes")                           
        return dict_graphe
    

    def  xlinkUniprotBis(self):    
        """
        Returns a dictionary like the previous one but with the proteins present in the interactome
        :rtype: dict
        """
        dict_graphe = {}
        with open("dict_graphe_uniprotBis.pkl", "wb") as pklFile:
            for protein in self.int_dict:
                for key, val in test_proteom.protID_prot_dict.items():
                    if val == protein:
                        dict_graphe[protein] = {'neighbours': self.int_dict[protein], 'uniprotID': test_proteom.prot_protID_dict[val]}
            pickle.dump(dict_graphe, pklFile)
            
        return dict_graphe
    
    
    def getProteinDomains(self, p):
        """
        Searches the page Uniprot of the p uniprotID and returns the name of the domains contained by this protein
        :param: uniprot_ID
        return : list containing the domains of protein
        """
        url = 'https://www.uniprot.org/uniprot/' + str(p) + '.txt'

        try:
            with urllib.request.urlopen(url) as response:
                
                with tempfile.TemporaryFile() as temp_file:
                    temp_file.write(response.read())     
                    temp_file.seek(0)
                    tmp = temp_file.read()
                    #recherche du domaine dans le ficher temporaire
                    domains_found = re.findall(r'(?<=Pfam; )\w+', str(tmp)) # ?<= retrouve les caractères qui viennent après Pfam;  W:lettre, chiffre et _
                    
                    #print(str(tmp))
                    domain_list = []
                    # Add the domains found in a list
                    
                    for domain in domains_found:
                        domain_list.append(domain)
                        
            return domain_list
    
        except Exception as error:  #ssl.SSLError, urllib.error.URLError
            #self.getProteinDomains(p)
            print(error)
            return None                         
            
    def xlinkdomains(self):
        """
        Add domains for each protein
        :return: dict containing the protein, its neighbors, its uniprot ID and its domains
        :rtype: dict
        """
        dict_graphe_final = self.xlinkUniprotBis()
        with open("dict_graphe_final.pkl", "wb") as pklFile:
            for key, val in dict_graphe_final.items(): # key:protein, val:uniprotID and neighboors
                domain = self.getProteinDomains(val['uniprotID'])
                #print(domain)
                dict_graphe_final[key]['domains'] = domain   
                
            pickle.dump(dict_graphe_final, pklFile)
            
        return dict_graphe_final


    #### week6: Analyze the composition of the interactome in domains #####


    def ls_proteins(self):
        """
        Returns a list of all the proteins present in the interaction graph protein
        :rtype: list
        """    
        protein_list = []
        
        #get proteins in int_dict
        for key in self.int_dict.keys():
            if not key in protein_list:
                protein_list.append(key)
            
        return protein_list
    
    def ls_domains(self):
        """
        Returns a list of all the domains non-redundant present in the interaction graph protein
        :rtype: list
        """
        domains_list = []
        dict_graphe = self.xlinkdomains()
        
        #get domains in dict_graph
        with open("list_domains.pkl", "wb") as pklFile:
            for key, val in dict_graphe.items():
                for domain in val['domains']:
                    if not domain in domains_list:
                        domains_list.append(domain)
            
            pickle.dump(dict_graphe, pklFile)
        return domains_list
            
            
    def ls_domains_n_init(self):
        """
        Returns a list of all the domains present in the interaction graph protein
        :rtype: list
        """
        domains_list = []
        dict_graphe = self.xlinkdomains()
        
        #get domains in dict_graph
        for key, val in dict_graphe.items():
            for domain_prot in val['domains']:
                domains_list.append(domain_prot)
            
        return domains_list
     
     
    def ls_domains_n(self, n):
        """
        Returns the list non-redundant of all domains found at least n times in proteins
        """
        domain_n_list = []
        domains_list = self.ls_domains_n_init()
        #print(domains_list)
        for domain in domains_list:
            if domains_list.count(domain) >= n and domain not in domain_n_list:
                domain_n_list.append(domain)
              
        return domain_n_list
    

    
    def prot_domains_nbDomains(self):
        """
        Returns a dictionnary whose keys are proteins and the value associated with each key are the list of protein domains
        and the number of protein comprising this number of domains
        :rtype: dict
        """
        try:
            dict_prot_domains = {}

            dict_graphe = self.xlinkdomains()
            
                #get domains in dict_graph
            for key, val in dict_graphe.items():

                if not key in dict_prot_domains:
                    dict_prot_domains[key] = {'domains': val['domains'], 'nb-domains': len(val['domains'])}                    
            return dict_prot_domains  
        
        except Exception as error:  
            print(error)
            return None  
    
    def nbDomainsByProteinDistribution(self):
        """
        returns a dictionary whose keys correspond to the number of domainsand the value associated with each key
        is the number of proteins comprising this number of domains
        :rtype: dict
        """
        list_nb_domains = []
        dict_nbDomains = {}
        dict_prot_domains = self.prot_domains_nbDomains()
        for key, val in dict_prot_domains.items():
            list_nb_domains.append(len(val['domains']))
            
        for nbDomains in list_nb_domains:
            if not nbDomains in dict_nbDomains:
                dict_nbDomains[nbDomains] = list_nb_domains.count(nbDomains)
                
        return dict_nbDomains
            
            
    def histDomainsByProteinDistribution(self):
        """
        Histrogramme to view the distribution of Domains by Proteins
        """
        dict_nbDomains = self.nbDomainsByProteinDistribution()
        nb_domains = dict_nbDomains.keys()
        nb_proteins = dict_nbDomains.values()
        plt.bar(nb_domains, nb_proteins)
        plt.xlabel("Number of domains")
        plt.ylabel("Number of proteins")
        plt.show()
        
              
    def  co_occurrence(self, dom_x, dom_y):
        """
        calculates and returns the number of co-occurrences of the x and y domains in the proteins of the interactome
        :param a: dom_x
        :param b: dom_y
        :rtype: int
        """
        nb_cooccurrence = 0
        
        dict_graphe = self.xlinkdomains() # prot, neighboors, uniprotID and domains
        for protein in dict_graphe.keys():
            if dom_x in dict_graphe[protein]["domains"] and dom_y in dict_graphe[protein]["domains"]: # verification if the 2 domains are presents in the prot
                nb_cooccurrence += 1
        return(nb_cooccurrence) 
    
    
    def get_prot_largest_nbDomains(self):
        """
        Returns the 10 proteins with the largest number of domains
        :rtype: list
        """
        dict_protNbDomains = self.prot_domains_nbDomains()           
        dict_prot_nb_domain = {}
        list_prot = []

        with open("prot_largest_nbDomains.pkl", "wb") as pklFile:
            for prot, dom in dict_protNbDomains.items():
                #get the number of domain of each prot
                dict_prot_nb_domain[prot] = len(dom['domains'])
                # sorts dictionary values ​​from smallest to largest and returns the last 10
                dict_prot_nb_domainBis = sorted(dict_prot_nb_domain.items(), key=lambda x: x[1], reverse=True)[:10]
            for prot in dict_prot_nb_domainBis:
                #print(prot)
                list_prot.append(prot[0])
            
            pickle.dump(list_prot, pklFile)
        return list_prot
                
        
    def get_prot_lowest_nbDomains(self):
        """
        Returns the 10 proteins with the lowest number of domains
        :rtype: list
        """
        dict_protNbDomains = self.prot_domains_nbDomains()           
        dict_prot_nb_domain = {}
        list_prot = []
        with open("prot_lowest_nbDomains.pkl", "wb") as pklFile:
            for prot, dom in dict_protNbDomains.items():
                #get the number of domain of each prot
                dict_prot_nb_domain[prot] = len(dom['domains'])
                # sorts dictionary values ​​from smallest to largest and returns the first 10
                dict_prot_nb_domainBis = sorted(dict_prot_nb_domain.items(), key=lambda x: x[1])[:10]
            for prot in dict_prot_nb_domainBis:
                list_prot.append(prot[0])
                
            pickle.dump(list_prot, pklFile)
        return list_prot        
        
        
        
        

interactome_file = Interactome('Files/Human_HighQuality.txt')
test_interactome = Interactome('Files/toy_example.txt')
test_interactomeBis = Interactome('Files/toy_exampleBis.txt')
interactome_file_test = Interactome('Files/Human_test.txt')


#print(test_interactome.int_dict)
#print(interactome_file.int_list)
#print(interactome_file.proteins)
#print(test_interactome.count_edges())
#print(test_interactomeBis.count_vertices())
#print(test_interactome.clean_interactome())
#print(test_interactome.get_degree('B'))
#print(test_interactome.get_max_degree())
#print(test_interactomeBis.get_ave_degree())
#print(test_interactome.histogram_degree(0, 5))
#print(test_interactomeBis.extractCC('B'))
#print(interactome_file.extractCC('1433B_HUMAN'))
#print(test_interactomeBis.get_ccs_dict())
#print(test_interactomeBis.countCC())
#print(interactome_file.writeCC())
#print(test_interactomeBis.computeCC())
#print("Density", test_interactomeBis.density())
#print(interactome_file.xlinkUniprot())
#print(interactome_file.xlinkUniprotBis())
#print(interactome_file.getProteinDomains('Q9Y5I7'))
#print(interactome_file.getProteinDomains('P56385'))
#print(interactome_file.xlinkdomains())
#print(interactome_file_test.ls_proteins())
#print(interactome_file.ls_domains())
#print(interactome_file.ls_domains_n_init())
#print(interactome_file.ls_domains_n(1))
#print(interactome_file.prot_domains_nbDomains())
#print(interactome_file.nbDomainsByProteinDistribution())
#print(interactome_file.histDomainsByProteinDistribution())
#print(interactome_file.co_occurrence('PF06741', 'PF07145'))
#print(interactome_file.prot_nbDomains())
#print(interactome_file.get_prot_largest_nbDomains())
print(interactome_file.get_prot_lowest_nbDomains())
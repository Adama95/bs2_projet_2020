#!/usr/bin/python3
# encoding: utf-8


import unittest
import oop_interaction_graph as ig
import proteome
import domain_graph

test_file = "C:\\Users\\adama\\OneDrive\\bs2_projet_2020\\Files\\toy_example.txt"
# Vérifie que first et second sont égaux.
class tests_functions(unittest.TestCase):
    
    def test_interaction_file_dict(self):
        dict_type = type(ig.Interactome.int_dict)
        self.assertEqual(dict_type, dict)  # Vérify if dict_type and dict are equals
           
    def test_interaction_file_list(self):
        list_type = type(ig.Interactome.int_list)
        self.assertEqual(list_type, list)
        
    def test_count_vertices(self):
        result_vertices = 6
        self.assertEqual(result_vertices, (ig.test_interactome.count_vertices()))
    
    def test_count_edges(self):
        result_edges = 6
        self.assertEqual(result_edges, (ig.test_interactome.count_edges()))
        
    def test_get_degree(self):
        result_degree = 3
        self.assertEqual(result_degree, (ig.test_interactome.get_degree('B')))

    def test_get_max_degree(self):
        resut_max_degree = ('B', 3, 'D', 3)
        self.assertEqual(resut_max_degree, (ig.test_interactome.get_max_degree()))
        
    def get_ave_degree(self):
        result_ave_degree = 1.8461538461538463
        self.assertEqual(result_ave_degree, (ig.test_interactomeBis.get_ave_degree()))        
        
    def test_get_ccs_dict(self):
        result_ccs_dict = [['A', 'B', 'C', 'D', 'E', 'F'], ['G', 'H', 'I'], ['J', 'K', 'L', 'M']]
        self.assertEqual(result_ccs_dict, (ig.test_interactomeBis.get_ccs_dict()))
        
    def test_extractCC(self):
        list_type = type(ig.test_interactomeBis.extractCC('B'))
        self.assertEqual(list_type, list)
        
    def test_density(self):
        result_density = 0.15384615384615385
        self.assertEqual(result_density, (ig.test_interactomeBis.density()))
        
    def test_xlinkUniprot(self):
        result_type = type(ig.interactome_file.xlinkUniprot())
        self.assertEqual(result_type, dict)
        
    def test_getProteinDomains(self):
        result_domain = ['PF00822']
        self.assertEqual(result_domain, (ig.interactome_file.getProteinDomains('Q9Y5I7')))
        
    def test_xlinkdomains(self):
        result_type = type(ig.interactome_file.xlinkdomains())
        self.assertEqual(result_type, dict)

    def test_ls_proteins(self):
      result_protein  = ['1433B_HUMAN', '1433E_HUMAN', '1433G_HUMAN', 'AKP13_HUMAN', 'BAD_HUMAN']
      self.assertEqual(result_protein, (ig.interactome_file_test.ls_proteins()))

    def test_ls_domains(self):
        result_type = type(ig.interactome_file.ls_domains())
        self.assertEqual(result_type, list)
        

        
if __name__ == '__main__':
    unittest.main()